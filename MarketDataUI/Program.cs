﻿using System;
using System.Windows.Forms;

using MarketDataUI.Common;
using MarketDataUI.Common.Logger;
using MarketDataUI.Common.TaskQueue;
using MarketDataUI.Models;

namespace MarketDataUI
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //setup services
            //simple logging service
            var logService = new DefaultLoggingService();
            ServiceLocator.Register<ILoggerService>(logService);

            //dispatcher to dispatch short running tasks on a single dedicated thread
            var dispatcher = new DefaultTaskDispatcherService();
            ServiceLocator.Register<ITaskDispatcherService>(dispatcher);

            //instrument caching service
            var instrumentService = new InstrumentService();
            ServiceLocator.Register<IInstrumentService>(instrumentService);
            //marekt data service
            var marketDataService = new DefaultMarketDataService();
            ServiceLocator.Register<IMarketDataService>(marketDataService);

            try
            {
                dispatcher.Start();
                Application.Run(new PricesUi());
            }
            finally
            {
                dispatcher.Stop();
            }       
        }
    }
}
