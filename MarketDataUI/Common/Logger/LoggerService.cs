﻿
namespace MarketDataUI.Common.Logger
{
    public class DefaultLoggingService : ILoggerService
    {
        private readonly ILogger _defaultLogger;
        
        public DefaultLoggingService()
        {
            _defaultLogger = new TraceLogger();
        }

        public ILogger CreateLogger()
        {
            return _defaultLogger;
        }
    }
}
