﻿
namespace MarketDataUI.Common.Logger
{
    public interface ILoggerService
    {
        ILogger CreateLogger();
    }
}
