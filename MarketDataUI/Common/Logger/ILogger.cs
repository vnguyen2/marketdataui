﻿namespace MarketDataUI.Common.Logger
{
    public enum LogLevelEnum
    {
        None,
        Debug,
        Info,
        Error
    }

    public interface ILogger
    {
        void Debug(string msg);
        void Error(string msg);
        void Info(string msg);
        void Log(LogLevelEnum level, string msg);
    }
}
