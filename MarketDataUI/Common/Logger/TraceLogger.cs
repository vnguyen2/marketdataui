﻿using System;
using System.Diagnostics;

namespace MarketDataUI.Common.Logger
{
    //A very basic logging facility using Trace
    public class TraceLogger : ILogger
    {
        public TraceLogger()
        {
            Trace.Listeners.Add(new TextWriterTraceListener(Console.Out));
            Trace.AutoFlush = true;
            Trace.Indent();
        }

        public void Debug(string msg)
        {
            Log(LogLevelEnum.Debug, msg);
        }

        public void Error(string msg)
        {
            Log(LogLevelEnum.Error, msg);
        }

        public void Info(string msg)
        {
            Log(LogLevelEnum.Info, msg);
        }

        public void Log(LogLevelEnum level, string msg)
        {
            var now = DateTime.Now.ToString("yyyyMMdd-hh:mss:ss.fff");
            var logLevel = level.ToString();
            Trace.WriteLine($"{now}[{logLevel}]:{msg}");
        }
    }
}
