﻿using System;

namespace MarketDataUI.Common.TaskQueue
{
    public class TaskItem
    {
        public DateTime QueuedTime { get; }

        public DateTime ProcessedTime { get; private set; }

        public Action Action { get; }

        public TaskItem(Action action)
        {
            QueuedTime = DateTime.UtcNow;
            ProcessedTime = DateTime.MinValue;
            Action = action;
        }

        public void Process()
        {
            if (Action != null)
            {
                ProcessedTime = DateTime.UtcNow;
                Action();
            }
        }
    }

    public interface ITaskDispatcherService
    {
        void Start();
        void Stop();
        bool Post(TaskItem item);
    }
}
