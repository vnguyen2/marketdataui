﻿using MarketDataUI.Common.Logger;
using System.Diagnostics.Contracts;
using System;
using System.Collections.Concurrent;
using System.Threading;

namespace MarketDataUI.Common.TaskQueue
{
    /// <summary>
    /// this dispatcher is to execute series of sort running tasks, that either need to be done in sequence or requires threadsafe
    /// </summary>
    public class DefaultTaskDispatcherService : ITaskDispatcherService
    {
        /// <summary>
        /// the dispatcher thread
        /// </summary>
        private readonly Thread _dispatcherThread;

        /// <summary>
        /// producer-consumer queue
        /// </summary>
        private readonly BlockingCollection<TaskItem> _actions;

        /// <summary>
        /// default logger
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// max queue size that we don't want to go over
        /// tasks will be dropped from the queue
        /// </summary>
        private readonly int _maxQueueSize;

        /// <summary>
        /// approximated count of pending tasks to be dispatched
        /// </summary>
        public int PendingCount => _actions.Count;

        /// <summary>
        /// whether the dispatcher is started, this property not threadsafe
        /// </summary>
        public bool IsStarted { get; private set; }

        /// <summary>
        /// whether the dispatcher is finished
        /// </summary>
        public bool IsCompleted => _actions.IsCompleted;

        public DefaultTaskDispatcherService(int queueSize = int.MaxValue)
        {
            _maxQueueSize = queueSize;
            var loggingService = ServiceLocator.Get<ILoggerService>();
            Contract.Ensures(loggingService != null, "Logging service should have been created!");
            _logger = loggingService.CreateLogger();
            _dispatcherThread = new Thread(Dispatch);
            _actions = new BlockingCollection<TaskItem>();
        }

        /// <summary>
        /// this method is non-blocking and not threadsafe
        /// expected to be called on the same thread with Stop()
        /// </summary>
        public void Start()
        {
            if (!IsStarted)
            {
                IsStarted = true;
                _logger.Info("TaskExecutor Thread starting.");
                _dispatcherThread.Start();
            }
        }

        /// <summary>
        /// this method is non-blocking and not threadsafe
        ///  expected to be called on the same thread with Start()
        /// </summary>
        public void Stop()
        {
            if (IsStarted)
            {
                //this causes the dispatcher to stop rather than the IsStarted flag itself
                _actions.CompleteAdding();
                _logger.Info("TaskExecutor Thread finished.");
                IsStarted = false;
            }
        }

        /// <summary>
        /// post an action to be run on the dispatcher
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public bool Post(TaskItem action)
        {
            if (!IsStarted)
            {
                _logger.Error("TaskExecutor Thread not running, this may overflow the queue");
                return false;
            }

            if (_actions.Count < _maxQueueSize)
            {
                _actions.Add(action);
                return true;
            }
            

            //TODO: should log every few minutes only
            _logger.Error("Consumer cannot keep up with producer, dropping tasks now...");
            return false;
        }

        private void Dispatch()
        {
            while (true)
            {
                try
                {
                    var task = _actions.Take();
                    task.Process();
                }
                catch (InvalidOperationException)
                {
                    _logger.Info("Dispatcher exiting ...");
                    break;
                }
            }

            _actions.Dispose();
        }
    }
}
