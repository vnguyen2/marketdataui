﻿using System;
using System.Collections.Generic;

namespace MarketDataUI.Common
{
    /// <summary>
    /// a simple ServiceLocator to register/retrive services
    /// </summary>
    public class ServiceLocator
    {
        private static readonly Dictionary<Type, object> Services;

        static ServiceLocator()
        {
            Services = new Dictionary<Type, object>();
        }

        /// <summary>
        /// register a service, can force to replace an existing service
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="service"></param>
        /// <param name="forced"></param>
        public static void Register<T>(T service, bool forced = false)
        {
            var t = typeof(T);
            if (forced || !Services.TryGetValue(t, out object s))
            {
                Services[t] = service;
            }
            else
            {
                throw new InvalidOperationException($"Service of type {t} is already registered.");
            }
        }

        /// <summary>
        /// get back a service
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T Get<T>(bool throwIfNotFound = false)
        {
            if (Services.TryGetValue(typeof(T), out object service))
            {
                return (T)service;
            }

            if (throwIfNotFound)
            {
                throw new InvalidOperationException($"Service of type {typeof(T)} not found.");
            }
            return default(T);
        }
    }
}
