﻿using System;

namespace MarketDataUI.Models
{
    [Flags]
    public enum MarketInfoFieldEnum
    {
        None,
        BidPrice = 1 << 1,
        BidQty = 1 << 2,
        AskPrice = 1 << 3,
        AskQty = 1 << 4
    }

    /// <summary>
    /// TODO: prefer struct for market data info as on the hotpath we can (possibly) use memory from the stack, 
    /// and a few assignment of literal types can be faster than memory lookup when cache missed
    /// however, if we do it in pure .Net then its implementation dependent and no guarantee  that struct is allocated on stack
    /// </summary>
    public class MarketInfo
    {
        public int InstrumentId;
        public decimal BidPrice;
        public uint BidQty;
        public decimal AskPrice;
        public uint AskQty;
    }
}
