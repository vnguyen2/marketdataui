﻿using MarketDataService;

namespace MarketDataUI.Models
{
    public interface IMarketDataService
    {
        IPriceClient CreatePriceClient();    
    }
}
