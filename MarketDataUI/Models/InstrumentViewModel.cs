﻿
namespace MarketDataUI.Models
{
    /// <summary>
    /// the instrument model for UI displaying
    /// </summary>
    public class InstrumentViewModel
    {
        private readonly object _lock = new object();
        private readonly MarketInfo _marketData;
        private MarketInfoFieldEnum _updatedFieldsEnum;
        public string Symbol { get; }

        public int InstrumentId => _marketData.InstrumentId;
        public uint BidQty => _marketData.BidQty;
        public uint AskQty => _marketData.AskQty;

        public decimal BidPrice
        {
            get
            {
                //avoid torn read
                lock(_lock)
                {
                    return _marketData.BidPrice;
                }
            }
        }

        public decimal AskPrice
        {
            get
            {
                //avoid torn read
                lock(_lock)
                {
                    return _marketData.AskPrice;
                }
            }
        }

        public bool IsCrossed
        {
            get
            {
                lock (_lock)
                {
                    return _marketData.BidPrice >= _marketData.AskPrice;
                }
            }
        }

        public InstrumentViewModel(string symbol, MarketInfo model)
        {
            Symbol = symbol;
            _marketData = model;
        }

        public void Update(MarketInfo updatedMarketInfo)
        {
            lock (_lock)
            {
                if (updatedMarketInfo.AskPrice != _marketData.AskPrice)
                {
                    _marketData.AskPrice = updatedMarketInfo.AskPrice;
                    _updatedFieldsEnum |= MarketInfoFieldEnum.AskPrice;
                }
                if (updatedMarketInfo.AskQty != _marketData.AskQty)
                {
                    _marketData.AskQty = updatedMarketInfo.AskQty;
                    _updatedFieldsEnum |= MarketInfoFieldEnum.AskQty;
                }
                if (updatedMarketInfo.BidPrice != _marketData.BidPrice)
                {
                    _marketData.BidPrice = updatedMarketInfo.BidPrice;
                    _updatedFieldsEnum |= MarketInfoFieldEnum.BidPrice;
                }
                if (updatedMarketInfo.BidQty != _marketData.BidQty)
                {
                    _marketData.BidQty = updatedMarketInfo.BidQty;
                    _updatedFieldsEnum |= MarketInfoFieldEnum.BidQty;
                }
            }
        }

        /// <summary>
        /// check and swap the update result
        /// </summary>
        /// <returns></returns>
        public MarketInfoFieldEnum CheckAndClearUpdate()
        {
            lock (_lock)
            {
                var updatePending = _updatedFieldsEnum;
                _updatedFieldsEnum = MarketInfoFieldEnum.None;
                return updatePending;
            }
        }
    }
}
