﻿using MarketDataService;
using System;

namespace MarketDataUI.Models
{
    public class InstrumentUpdatedEventArgs : EventArgs
    {
        public string Symbol { get; }
        public MarketInfo CurrentMarketInfo { get; }
        public MarketInfo NewMarketInfo { get; }

        public InstrumentUpdatedEventArgs(string symbol, MarketInfo newMarketInfo, MarketInfo existingInst = default(MarketInfo))
        {
            Symbol = symbol;
            CurrentMarketInfo = existingInst;
            NewMarketInfo = newMarketInfo;
        }
    }

    public interface IInstrumentService
    {
        event EventHandler<InstrumentUpdatedEventArgs> InstrumentAdded;
        event EventHandler<InstrumentUpdatedEventArgs> InstrumentUpdated;
        void ProcessInstrumentUpdate(object sender, PriceChangedEventArgs e);
    }
}
