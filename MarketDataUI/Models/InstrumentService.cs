﻿using MarketDataService;
using System;
using System.Collections.Generic;
using MarketDataUI.Common.TaskQueue;
using MarketDataUI.Common;

namespace MarketDataUI.Models
{
    /// <summary>
    /// InstrumentService caches all known instruments
    /// </summary>
    public class InstrumentService : IInstrumentService
    {
        /// <summary>
        /// a new instrument is added together with market data
        /// </summary>
        public event EventHandler<InstrumentUpdatedEventArgs> InstrumentAdded;

        /// <summary>
        /// an existing instrument market data is updated
        /// </summary>
        public event EventHandler<InstrumentUpdatedEventArgs> InstrumentUpdated;

        /// <summary>
        /// the default dispatcher
        /// </summary>
        private readonly ITaskDispatcherService _dispatcher;

        /// <summary>
        /// the cache of all known instruments
        /// </summary>
        private readonly Dictionary<string, MarketInfo> _instCache;

        public InstrumentService()
        {
            _instCache = new Dictionary<string, MarketInfo>();
            _dispatcher = ServiceLocator.Get<ITaskDispatcherService>();
            if (_dispatcher == null)
                throw new InvalidOperationException("No dispatcher registered!");
        }

        /// <summary>
        /// handle market data update on the dispatcher thread
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ProcessInstrumentUpdate(object sender, PriceChangedEventArgs e)
        {
            var newInst = new MarketInfo()
            {
                AskPrice = e.AskPrice,
                AskQty = e.AskQty,
                BidPrice = e.BidPrice,
                BidQty = e.BidQty
            };

            var taskItem = new TaskItem(
                () =>
                {
                    if (!_instCache.TryGetValue(e.Symbol, out MarketInfo foundInst))
                    {
                        _instCache[e.Symbol] = newInst;

                        InstrumentAdded?.Invoke(this, new InstrumentUpdatedEventArgs(e.Symbol, newInst));
                    }
                    else
                    {
                        InstrumentUpdated?.Invoke(this, new InstrumentUpdatedEventArgs(e.Symbol, newInst, foundInst));
                    }
                });

            _dispatcher.Post(taskItem);
        }
    }
}
