﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MarketDataUI.Models
{
    /// <summary>
    /// this holds the list of all instrument models
    /// it is accessed by two threads: the dispatcher thread and the UI thread
    /// the dispatcher thread updates/adds new items
    /// the UI thread reads the item content
    /// for each element in the list, update to the element content (i.e the InstrumentViewModel) is protected via a lock
    /// for the list, it is threadsafe to access an element reference if the list is not resized  when it runs out of its reserved size
    /// to avoid additional locking, this class uses a custom list of arrays of fixed size and keep adding new array to save the cost
    /// of an addition lock
    /// when the list is resize, only the reference of the arrays are recopied, not the arrays' contents themselves
    /// </summary>
    public class InstrumentList
    {
        private const int ArraySizeBits = 6; 
        const int ArraySize = 1 << ArraySizeBits; 
        private readonly List<InstrumentViewModel[]> _list;

        public int Count { get; private set; }

        public InstrumentList()
        {
            _list = new List<InstrumentViewModel[]>();
        }

        public InstrumentViewModel this[int index]
        {
            get
            {
                if (index < Count)
                {
                    return GetIndex(index, out int _);
                }

                throw new IndexOutOfRangeException($"{index} >= {Count}");
            }
        }

        public void Add(string symbol, MarketInfo marketData)
        {
            marketData.InstrumentId = Count;
            int index = (Count & (ArraySize - 1));
            if (index == 0)
            {
                var newArray = new InstrumentViewModel[ArraySize];
                _list.Add(newArray);
            }

            _list.Last()[index] = new InstrumentViewModel(symbol, marketData);

            ++Count;
        }

        public void UpdateInstrument(MarketInfo existing, MarketInfo newInst)
        {
            this[existing.InstrumentId].Update(newInst);
        }

        private InstrumentViewModel GetIndex(int index, out int arrayIndex)
        {
            arrayIndex = index >> ArraySizeBits;
            int indexInArray = index - (arrayIndex << ArraySizeBits);
            return _list[arrayIndex][indexInArray];
        }
    }
}
