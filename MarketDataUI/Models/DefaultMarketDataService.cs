﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MarketDataService;

namespace MarketDataUI.Models
{
    public class DefaultMarketDataService : IMarketDataService
    {
        public IPriceClient CreatePriceClient()
        {
            return new PriceClient();
        }
    }

    /*
    public class TestPriceClient : IPriceClient
    {
        private readonly PriceChangedEventArgs[] _args = new PriceChangedEventArgs[400];
        public bool IsRunning { get; private set; }
        public event EventHandler<PriceChangedEventArgs> OnPriceChanged;

        public void Start(int sleepMilliseconds = 1)
        {
            string[] symbols = new string[_args.Length];
            for (int i = 0; i < symbols.Length; i++)
            {
                symbols[i] = "Symbol" + i;
            }

            IsRunning = true;
            Task.Run(() => Run(symbols, true));
            Task.Run(() => Run(symbols, false));
        }

        private void Run(string[] symbols, bool oddIndex, int sleepMilliseconds = 1)
        {
            var rand = new Random();
            while (true)
            {
                int index = rand.Next((symbols.Length - 1)/2);
                index = index * 2;
                if (oddIndex) index++;

                if (IsRunning)
                {
                    uint refVal = (uint) rand.Next(1000);
                    _args[index] = new PriceChangedEventArgs(symbols[index], refVal + (uint)index,
                        refVal - index, refVal + index, refVal + (uint)index);
                    OnPriceChanged?.Invoke(this, _args[index]);
                    Thread.Sleep(sleepMilliseconds);
                }
                else
                {
                    Thread.Sleep(100);
                }
            }
        }

        public void Stop()
        {
            IsRunning = false;
        }
}*/
}
