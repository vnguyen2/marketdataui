﻿using System;
using System.Windows.Forms;

namespace MarketDataUI.Models
{
    /// <summary>
    /// Presenter class to retrieve value for a gridcell and decorate the cell
    /// </summary>
    public class GridCellPresenter
    {
        private Func<InstrumentViewModel, object> _getValue;
        private Action<DataGridViewCellStyle, InstrumentViewModel> _decorate;
        
        public object RetrieveValue(InstrumentViewModel inst)
        {
            return _getValue?.Invoke(inst);
        }

        public void Decorate(InstrumentViewModel model, DataGridViewCellStyle style)
        {
            _decorate?.Invoke(style, model);
        }

        /// <summary>
        /// retrieve a cell value
        /// </summary>
        /// <param name="getter"></param>
        /// <returns></returns>
        public GridCellPresenter SetValueGetter(Func<InstrumentViewModel, object> getter)
        {
            _getValue = getter;
            return this;
        }

        /// <summary>
        /// decorate a cell based on its value
        /// </summary>
        /// <param name="decorator"></param>
        /// <returns></returns>
        public GridCellPresenter SetDecorator(Action<DataGridViewCellStyle, InstrumentViewModel> decorator)
        {
            _decorate = decorator;
            return this;
        }

        /// <summary>
        /// decoreate a cell without using its value
        /// </summary>
        /// <param name="decorator"></param>
        /// <returns></returns>
        public GridCellPresenter SetDecorator(Action<DataGridViewCellStyle> decorator)
        {
            _decorate = (style, model)=> decorator(style);
            return this;
        }
    }
}
