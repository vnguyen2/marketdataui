﻿using MarketDataService;
using MarketDataUI.Common;
using MarketDataUI.Common.Logger;
using MarketDataUI.Models;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Timer = System.Windows.Forms.Timer;

namespace MarketDataUI
{
    public partial class PricesUi : Form
    {
        private const int MarketDataEventTimeoutInMs = 1000;
        /// <summary>
        /// the list of instruments which is updated on the dispatcher thread
        /// </summary>
        private readonly InstrumentList _instruments = new InstrumentList();

        /// <summary>
        /// timer to repain the updated rows
        /// </summary>
        private readonly Timer _uiTimer = new Timer { Interval = 100 };
        private IPriceClient _marketListener;
        private ILogger _logger;
        private readonly SynchronizationContext _uiContext;

        public PricesUi()
        {
            InitializeComponent();
            _uiContext = SynchronizationContext.Current;

            SetupService();
            BuildColumns();

            PricesDataGridView.AutoGenerateColumns = true;
            PricesDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            PricesDataGridView.RowHeadersVisible = false;

            //prefer grid virtual mode to binding for performance
            PricesDataGridView.VirtualMode = true;
            PricesDataGridView.CellFormatting += GridCellFormatting;
            PricesDataGridView.CellValueNeeded += GridCellValueNeeded;
            _uiTimer.Tick += TimerTicked;
            pricesToolStripMenuItem.DropDownOpening += DecoratePriceStartStopMenu;
        }

        private void SetupService()
        {
            var instrumentService = ServiceLocator.Get<IInstrumentService>();
            var marketDataService = ServiceLocator.Get<IMarketDataService>();
            var logService = ServiceLocator.Get<ILoggerService>();
            _logger = logService.CreateLogger();

            _marketListener = marketDataService.CreatePriceClient();
            _marketListener.OnPriceChanged += instrumentService.ProcessInstrumentUpdate;
            instrumentService.InstrumentAdded += HandleInstrumentAdded;
            instrumentService.InstrumentUpdated += HandleInstrumentUpdated;
        }

        private void DecoratePriceStartStopMenu(object sender, EventArgs arg)
        {
            startToolStripMenuItem.Enabled = !_marketListener.IsRunning;
            stopToolStripMenuItem.Enabled = _marketListener.IsRunning;
        }

        private void BuildColumns()
        {
            //symbol column
            var symbolPresenter = new GridCellPresenter()
                .SetValueGetter(inst => inst.Symbol);
            AddColumn("Symbol", symbolPresenter);

            //bidqty column
            var bidQtyPresenter = new GridCellPresenter()
                .SetValueGetter(inst => inst.BidQty);
            AddColumn("BidQty", bidQtyPresenter);

            //bidprice column, color as red if crossed
            var bidPresenter = new GridCellPresenter()
                .SetValueGetter(inst => inst.BidPrice)
                .SetDecorator((style, inst) =>
                    {
                        style.ForeColor = inst.IsCrossed ? System.Drawing.Color.Red : System.Drawing.Color.Black;
                    }
                );

            AddColumn("BidPrice", bidPresenter);

            //askprice column, color as red if crossed
            var askPresenter = new GridCellPresenter()
            .SetValueGetter(inst => inst.AskPrice)
                .SetDecorator((style, inst) =>
                    {
                        style.ForeColor = inst.IsCrossed ? System.Drawing.Color.Red : System.Drawing.Color.Black;
                    }
            );

            AddColumn("AskPrice", askPresenter);

            //askqty column
            var askQtyPresenter = new GridCellPresenter()
                .SetValueGetter(inst => inst.AskQty);

            AddColumn("AskQty", askQtyPresenter);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            _uiTimer.Start();
        }

        protected override void OnClosed(EventArgs e)
        {
            StopMarketListener();
            base.OnClosed(e);
        }

        public void AddColumn(string header, GridCellPresenter cellPresenter)
        {
            DataGridViewTextBoxColumn newCol = new DataGridViewTextBoxColumn
            {
                HeaderText = header,
                Name = header,
                Tag = cellPresenter
            };
            PricesDataGridView.Columns.Add(newCol);
        }

        private void HandleInstrumentUpdated(object sender, InstrumentUpdatedEventArgs e)
        {
            _instruments.UpdateInstrument(e.CurrentMarketInfo, e.NewMarketInfo);
        }

        private void HandleInstrumentAdded(object sender, InstrumentUpdatedEventArgs e)
        {
            _instruments.Add(e.Symbol, e.NewMarketInfo);
        }

        /// <summary>
        /// get cell value on request
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {
            if (e.RowIndex <= _instruments.Count)
            {
                var cellPresenter = (GridCellPresenter) PricesDataGridView.Columns[e.ColumnIndex].Tag;
                var item = _instruments[e.RowIndex];
                e.Value = cellPresenter.RetrieveValue(item);
            }
        }

        /// <summary>
        /// format a cell on request
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex <= _instruments.Count)
            {
                var cellPresenter = (GridCellPresenter) PricesDataGridView.Columns[e.ColumnIndex].Tag;
                var item = _instruments[e.RowIndex];
                cellPresenter.Decorate(item, e.CellStyle);
            }
        }

        private void TimerTicked(object sender, EventArgs e)
        {
            PricesDataGridView.RowCount = _instruments.Count;
            var firstRow = PricesDataGridView.FirstDisplayedScrollingRowIndex;
            var rowCount = PricesDataGridView.DisplayedRowCount(true);

            for (int i = firstRow; i < firstRow + rowCount; i++)
            {
                var inst = _instruments[i];
                //by check and clear at the same time, we may repaint more than necessary when there is another update 
                //and we haven't retrieve the data to repaint the row yet
                //however this avoids the overcomplication of marking update for individual item fields
                if (inst.CheckAndClearUpdate() != MarketInfoFieldEnum.None)
                {                  
                    //as metioned above, since there are only a few columns, we repain the whole row
                    //otherwise, we can determine down to the cell level which ones to repaint individually
                    PricesDataGridView.InvalidateRow(i);                  
                }
            }
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Start the flow of price data to the client
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StartMarketListener();
        }

        /// <summary>
        /// Stop the flow of price data to the client
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StopMarketListener();
        }

        private void StartMarketListener()
        {
            var startTask =Task.Run(
            () =>
            {
                _logger.Debug("Price Client starting...!");
                _marketListener.Start();
            });

            
            TimeSpan ts = TimeSpan.FromMilliseconds(MarketDataEventTimeoutInMs);
            if (!startTask.Wait(ts))
            {
                //_uiContext.Post(state => { MessageBox.Show("Timeout to start the markte listener", "MarketDataUI", MessageBoxButtons.OK); }, null);

                _logger.Error("Timeout waiting for Price Client to start");
            }
        }

        private void StopMarketListener()
        {
            var stopTask = Task.Run(
               () =>
               {
                   _marketListener.Stop();
                   _logger.Debug("Price Client stopped...!");
               }
               );

            var ts = TimeSpan.FromMilliseconds(MarketDataEventTimeoutInMs);
            if (!stopTask.Wait(ts))
            {
                //for MessageBox, we don't need it to run on UI thread
                //this is just for convenient if we want to decide not using MessageBox later
                _uiContext.Post(state => { MessageBox.Show("Timeout to stop the markte listener", "MarketDataUI", MessageBoxButtons.OK); }, null);
            }
        }
    }
}
