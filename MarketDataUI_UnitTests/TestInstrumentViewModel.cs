﻿using MarketDataUI.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MarketDataUI_UnitTests
{
    [TestClass]
    public class TestInstrumentViewModel
    {
        [TestMethod]
        public void TestConstructor()
        {
            var marketInfo = new MarketInfo()
            {
                AskPrice = 46m,
                BidPrice = 45m,
                AskQty = 100,
                BidQty = 5
            };
            var testObject = new InstrumentViewModel("BHP.AX", marketInfo);
            Assert.AreEqual(testObject.Symbol, "BHP.AX");
            Assert.AreEqual(marketInfo.AskPrice, testObject.AskPrice);
            Assert.AreEqual(marketInfo.BidPrice, testObject.BidPrice);
            Assert.AreEqual(marketInfo.AskQty, testObject.AskQty);
            Assert.AreEqual(marketInfo.BidQty, testObject.BidQty);
        }

        [TestMethod]
        public void TestUpdate()
        {           
            var testObject = new InstrumentViewModel("BHP.AX", new MarketInfo());

            var marketInfo = new MarketInfo()
            {
                AskPrice = 46m,
                BidPrice = 45m,
                AskQty = 100,
                BidQty = 5
            };

            testObject.Update(marketInfo);

            Assert.AreEqual(testObject.Symbol, "BHP.AX");
            Assert.AreEqual(marketInfo.AskPrice, testObject.AskPrice);
            Assert.AreEqual(marketInfo.BidPrice, testObject.BidPrice);
            Assert.AreEqual(marketInfo.AskQty, testObject.AskQty);
            Assert.AreEqual(marketInfo.BidQty, testObject.BidQty);
        }

        [TestMethod]
        public void TestCheckAndUpdate()
        {
            var testObject = new InstrumentViewModel("BHP.AX", new MarketInfo());

            var marketInfo = new MarketInfo()
            {
                AskPrice = 46m,
                BidPrice = 45m,
                AskQty = 100,
                BidQty = 5
            };

            testObject.Update(marketInfo);
            var update = testObject.CheckAndClearUpdate();
            Assert.AreEqual(MarketInfoFieldEnum.BidPrice | MarketInfoFieldEnum.AskPrice | MarketInfoFieldEnum.AskQty | MarketInfoFieldEnum.BidQty, update );

            marketInfo.AskPrice = 47m;
            testObject.Update(marketInfo);
            update = testObject.CheckAndClearUpdate();
            Assert.AreEqual(MarketInfoFieldEnum.AskPrice, update);

            marketInfo.BidPrice = 42m;
            testObject.Update(marketInfo);
            update = testObject.CheckAndClearUpdate();
            Assert.AreEqual(MarketInfoFieldEnum.BidPrice, update);

            marketInfo.AskQty = 1;
            testObject.Update(marketInfo);
            update = testObject.CheckAndClearUpdate();
            Assert.AreEqual(MarketInfoFieldEnum.AskQty, update);

            marketInfo.BidQty = 0;
            testObject.Update(marketInfo);
            update = testObject.CheckAndClearUpdate();
            Assert.AreEqual(MarketInfoFieldEnum.BidQty, update);

        }
    }
}
