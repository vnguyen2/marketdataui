﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MarketDataService;
using MarketDataUI.Common;
using MarketDataUI.Common.Logger;
using MarketDataUI.Common.TaskQueue;
using MarketDataUI.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace MarketDataUI_UnitTests
{
    [TestClass]
    public class TestInstrumentService
    {
        [TestInitialize]
        public void Setup()
        {
            var mockLogger = new Mock<ILogger>();
            var mocklogService = new Mock<ILoggerService>();
            mocklogService.Setup(mockService => mockService.CreateLogger()).Returns(mockLogger.Object);

            var mockDispatcherService = new Mock<ITaskDispatcherService>();
            mockDispatcherService.Setup(mockService => mockService.Start());
            mockDispatcherService.Setup(mockService => mockService.Stop());
            mockDispatcherService.Setup(mockService => mockService.Post(It.IsAny<TaskItem>()))
                .Callback((TaskItem task) => { task.Action(); });

            ServiceLocator.Register(mocklogService.Object, true);
            ServiceLocator.Register(mockDispatcherService.Object, true);
            mockDispatcherService.Object.Start();
        }

        [TestCleanup]
        public void Teardown()
        {
            ServiceLocator.Get<ITaskDispatcherService>().Stop();
        }

        [TestMethod]
        public void TestRegister()
        {
            var testObject = new InstrumentService();
            ServiceLocator.Register<IInstrumentService>(testObject, true);
            var registerObject = ServiceLocator.Get<IInstrumentService>();
            Assert.AreSame(testObject, registerObject);
        }

        [TestMethod]
        public void TestHandlePriceInstrumentAdd()
        {
            var testObject = new InstrumentService();
            InstrumentUpdatedEventArgs instArgs = null;
            testObject.InstrumentAdded += (sender, args) => { instArgs = args; };
            testObject.InstrumentUpdated += (sender, args) => { instArgs = args; };

            var receivedData = new PriceChangedEventArgs("BHP.AX", 1, 34.5m, 37.0m, 2);
            testObject.ProcessInstrumentUpdate(this, receivedData);
            CheckMatch(instArgs.NewMarketInfo, receivedData);
            Assert.IsNull(instArgs.CurrentMarketInfo);

            //another symbol
            receivedData = new PriceChangedEventArgs("ANZ.AX", 100, 28.56m, 28.990m, 200);
            testObject.ProcessInstrumentUpdate(this, receivedData);
            CheckMatch(instArgs.NewMarketInfo, receivedData);
            Assert.IsNull(instArgs.CurrentMarketInfo);
        }

        [TestMethod]
        public void TestHandlePriceInstrumentUpdate()
        {
            var testObject = new InstrumentService();
            InstrumentUpdatedEventArgs instArgs = null;

            testObject.InstrumentAdded += (sender, args) => { instArgs = args; };
            testObject.InstrumentUpdated += (sender, args) => { instArgs = args; };

            var receivedData = new PriceChangedEventArgs("BHP.AX", 1, 34.5m, 37.0m, 2);
            testObject.ProcessInstrumentUpdate(this, receivedData);
            CheckMatch(instArgs.NewMarketInfo, receivedData);
            Assert.IsNull(instArgs.CurrentMarketInfo);

            //another update
            var receivedData2 = new PriceChangedEventArgs("BHP.AX", 100, 39.56m, 39.990m, 200);
            testObject.ProcessInstrumentUpdate(this, receivedData2);
            CheckMatch(instArgs.NewMarketInfo, receivedData2);
            CheckMatch(instArgs.CurrentMarketInfo, receivedData);
            var existingInst = instArgs.CurrentMarketInfo;

            //update again
            var receivedData3 = new PriceChangedEventArgs("BHP.AX", 101, 39.57m, 39.980m, 201);
            testObject.ProcessInstrumentUpdate(this, receivedData3);
            CheckMatch(instArgs.NewMarketInfo, receivedData3);
            Assert.AreSame(instArgs.CurrentMarketInfo, existingInst);
        }

        [TestMethod]
        public void TestHandlePriceMultiThread()
        {
            var testObject = new InstrumentService();
            var addCount = 0;
            var updateCount = 0;
            testObject.InstrumentAdded += (sender, args) => { Interlocked.Increment(ref addCount); };
            testObject.InstrumentUpdated += (sender, args) => { Interlocked.Increment(ref updateCount); };


            var actions = new Action[10];
            for (uint i = 0; i < 5; i++)
            {
                var k = i;
                actions[i] = () =>
                {
                    var receivedDataBhp = new PriceChangedEventArgs("BHP.AX", 1 + k, 34.5m + k, 37.0m + k, 2 + k);
                    testObject.ProcessInstrumentUpdate(this, receivedDataBhp);
                };
                actions[i + 5] = () =>
                {
                    var receiveDataAnz = new PriceChangedEventArgs("ANZ.AX", 100 + k, 28.56m + k, 28.990m + k, 200 + k);
                    testObject.ProcessInstrumentUpdate(this, receiveDataAnz);
                };
            }

            Parallel.Invoke(actions);
            Assert.AreEqual(2, addCount);
            Assert.AreEqual(8, updateCount);
        }

        private void CheckMatch(MarketInfo inst, PriceChangedEventArgs args)
        {
            Assert.AreEqual(args.BidQty, inst.BidQty);
            Assert.AreEqual(args.BidPrice, inst.BidPrice);
            Assert.AreEqual(args.AskPrice, inst.AskPrice);
            Assert.AreEqual(args.AskQty, inst.AskQty);
        }
    }
}