﻿using System.Collections.Generic;
using MarketDataUI.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MarketDataUI_UnitTests
{
    [TestClass]
    public class TestInstrumentList
    {
        [TestMethod]
        public void TestConstructor()
        {
            var testObject = new InstrumentList();
            Assert.AreEqual(0, testObject.Count);
        }

        /// <summary>
        /// our custom list should acts like a list for adding 
        /// </summary>
        [TestMethod]
        public void TestAdd()
        {
            var testObject = new InstrumentList();
            var list = new List<MarketInfo>();

            for (var i = 0; i < 1000; i++)
            {
                var inst = new MarketInfo();
                testObject.Add("s" + i, inst);
                list.Add(inst);
            }

            Assert.AreEqual(list.Count, testObject.Count);
            for (var i = 0; i < list.Count; i++) CheckMarketData(list[i], testObject[i], (uint) i);
        }

        /// <summary>
        /// our custom list should acts like a list for adding and retrieving
        /// </summary>
        [TestMethod]
        public void TestIndexOperator()
        {
            var testObject = new InstrumentList();
            for (var i = 0; i < 1000; i++)
            {
                var marketInfo = new MarketInfo();
                testObject.Add("s" + i, marketInfo);
                Assert.AreEqual(i, testObject[i].InstrumentId, "Item index doesn't match!");
            }
        }

        [TestMethod]
        public void TestUpdate()
        {
            var testObject = new InstrumentList();
            for (uint i = 0; i < 1000; i++)
            {
                var symbol = "ASX" + i;
                var marketInfo = new MarketInfo
                {
                    BidPrice = 100.0m + i,
                    BidQty = 5 * i,
                    AskPrice = 20.5m + i,
                    AskQty = 2 * i
                };

                testObject.Add(symbol, marketInfo);
                var updateMarketInfo = new MarketInfo
                {
                    BidPrice = 100.0m + 2 * i,
                    BidQty = i,
                    AskPrice = 20.5m + i,
                    AskQty = 5 * i
                };

                testObject.UpdateInstrument(marketInfo, updateMarketInfo);
                CheckMarketData(updateMarketInfo, testObject[(int) i], i);

                //second update
                updateMarketInfo.BidPrice += i;
                updateMarketInfo.BidQty += i;
                updateMarketInfo.AskPrice += i;
                updateMarketInfo.AskQty += i;

                testObject.UpdateInstrument(marketInfo, updateMarketInfo);
                CheckMarketData(updateMarketInfo, testObject[(int) i], i);
            }
        }

        private void CheckMarketData(MarketInfo marketInfo, InstrumentViewModel inst, uint index)
        {
            Assert.AreEqual(marketInfo.BidPrice, inst.BidPrice, $"Item at index {index} bidprice doesn't match");
            Assert.AreEqual(marketInfo.BidQty, inst.BidQty, $"Item at index {index} bidqty doesn't match");
            Assert.AreEqual(marketInfo.AskPrice, inst.AskPrice, $"Item at index {index} askprice doesn't match");
            Assert.AreEqual(marketInfo.AskQty, inst.AskQty, $"Item at index {index} askprice doesn't match");
        }
    }
}