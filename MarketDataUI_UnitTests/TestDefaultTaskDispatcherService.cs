﻿using MarketDataUI.Common;
using MarketDataUI.Common.Logger;
using MarketDataUI.Common.TaskQueue;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading;

namespace MarketDataUI_UnitTests
{
    [TestClass]
    public class TestDefaultTaskDispatcherService
    {
        private DefaultTaskDispatcherService _testObject;

        [TestInitialize]
        public void Setup()
        {
            var mockLogger = new Mock<ILogger>();
            var mocklogService = new Mock<ILoggerService>();
            mocklogService.Setup(mockService => mockService.CreateLogger()).Returns(mockLogger.Object);          
            ServiceLocator.Register<ILoggerService>(mocklogService.Object, true);
            _testObject = new DefaultTaskDispatcherService();
        }

        [TestCleanup]
        public void Teardown()
        {
            _testObject?.Stop();
        }

        [TestMethod]
        public void TestStart()
        {
            var testObject = new DefaultTaskDispatcherService();
            testObject.Start();
            Thread.SpinWait(200);
            Assert.IsTrue(testObject.IsStarted);
        }

        [TestMethod]
        public void TestStop()
        {
            _testObject.Start();
            Thread.SpinWait(200);
            _testObject.Stop();
            Thread.SpinWait(200);
            Assert.IsTrue(_testObject.IsCompleted);           
        }

        /// <summary>
        /// test if tasks are executed in sequences when posted on the dispatcher
        /// </summary>
        [TestMethod]
        public void TestTaskExecutedInSeq()
        {
            _testObject.Start();
            int[] taskDone = new int[100];
            for (int i = 0; i < taskDone.Length; i++)
            {
                int k = i;
                _testObject.Post(new TaskItem(() => taskDone[k] = k));
            }

            while (_testObject.PendingCount > 0)
            {
                //wait for compeletion
                Thread.Sleep(1000);
            }


            //check execution should be in this order
            for(int i = 0; i < taskDone.Length;i++)
            {
                Assert.AreEqual(i, taskDone[i]);
            }
        }

        /// <summary>
        /// test if the consumer is too slow and cannot keep up with the updated rates
        /// </summary>
        [TestMethod]
        public void TestSlowConsumer()
        {
            _testObject = new DefaultTaskDispatcherService(5);
            _testObject.Start();

            bool dropTask = false;
            for (int i = 0; i < 10; i++)
            {
                dropTask = dropTask || _testObject.Post(new TaskItem(() => Thread.Sleep(1000)));
            }

            Assert.IsTrue(dropTask);
        }
    }
}
