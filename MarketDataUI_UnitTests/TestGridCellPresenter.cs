﻿using MarketDataUI.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarketDataUI_UnitTests
{
    [TestClass]
    public class TestGridCellPresenter
    {
        [TestMethod]
        public void TestDecorate()
        {
            GridCellPresenter testObject = new GridCellPresenter();
            testObject.SetDecorator((s, item) =>
            {
                s.BackColor = Color.Blue;
                s.ForeColor = Color.DarkGreen;
            });

            DataGridViewCellStyle style = new DataGridViewCellStyle();
            testObject.Decorate(new InstrumentViewModel("BHP.AX", new MarketInfo()), style);
            Assert.AreEqual(Color.Blue, style.BackColor);
            Assert.AreEqual(Color.DarkGreen, style.ForeColor);
        }

        [TestMethod]
        public void TestValueGetter()
        {
            GridCellPresenter testObject = new GridCellPresenter();
            var value = testObject.RetrieveValue(new InstrumentViewModel("BHP.AX", new MarketInfo()));
            Assert.IsNull(value);

            testObject.SetValueGetter(item => 9);
            value = testObject.RetrieveValue(new InstrumentViewModel("BHP.AX", new MarketInfo()));
            Assert.AreEqual(9, value);
        }
    }
}
